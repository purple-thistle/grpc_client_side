package main

import (
	"context"
	"fmt"
	"os"
	"time"

	pb "gitlab.com/purple-thistle/grpc_server_api"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	address = "localhost:50051"
)

func main() {

	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		fmt.Println("did not connect: " + err.Error())
		os.Exit(1)
	}
	defer conn.Close()

	newClient := pb.NewCreditCardClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	inputCC := new(pb.CreditCardRequest)
	inputCC.CreditCardNumber = "8273 1232 7352 0569"

	r, err := newClient.ValidateCreditCard(ctx, inputCC)
	if err != nil {

		os.Exit(1)
	}

	fmt.Println(r)
}
